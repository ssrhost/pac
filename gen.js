const fs = require('fs')
let content = fs.readFileSync('./mac-pac.txt', 'utf-8')

content = content.split('\n').map((item) => {
  if (item.indexOf('.') === 0 || !item) {
    return item
  }
  return '||' + item
}).join('\n')

console.log(content)

fs.writeFileSync('./list.txt', new Buffer('[AutoProxy 0.2.9]\n\n' + content).toString('base64'))
